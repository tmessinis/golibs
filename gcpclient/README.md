# About
Library that allows you communicate with GCP using default credentials, via GCP's REST api. At the moment it is meant to only obtain information of resources and not manipulate them. It generates a GcpClient struct with holds state and substructs within it that represent various GCP resources.

## Example usage
Obtain all GCP projects within an org:
```
package main

import (
  "fmt"
  "bitbucket.org/tmessinis/golibs/gcpclient"
)

func main() {
  gClient := gcpclient.NewGcpClient()
  gClient.GetProjects()
  fmt.Println(gClient.Projects)
}
```
