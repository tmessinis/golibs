package gcpclient

import (
  "fmt"
  "errors"
  "net/http"
  "context"
  "golang.org/x/oauth2/google"
  "google.golang.org/api/container/v1"
  "google.golang.org/api/cloudresourcemanager/v1"

  h "bitbucket.org/tmessinis/golibs/gohelpers"
)

// Struct that defines and maintains the state of a GCP client.
// Substructs within it hold various GCP resources.
type GcpClient struct {
  context     context.Context
  crmClient   *http.Client
  contClient  *http.Client
  crmSvc      *cloudresourcemanager.Service
  contSvc     *container.Service
  Projects    map[string]GcpProject
  GkeClusters map[string]GkeCluster
}

type GcpProject struct {
  Name   string
  Id     string
  Number int64
  Labels map[string]string
  Parent *cloudresourcemanager.ResourceId //map[string]string
}

type GkeCluster struct {
  Name   string
  Zone   string
  IpAddr string
  Labels map[string]string
}

// Constructor for a new GCP client object
func NewGcpClient(gcpResources ...string) *GcpClient {
  ctx := context.Background()
  gcpClient, err := New(ctx, gcpResources...)
  h.Check(err)
  return gcpClient
}

func New(ctx context.Context, gcpResources ...string) (*GcpClient, error) {
  if ctx == nil {
    return nil, errors.New("Context is nil")
  }
  gcpClient := GcpClient{context: ctx}

  // A read only cloud resource manager is set by default to read in all the GCP projects
  gcpClient.crmClient = NewCloudResourceManagerClientRO(gcpClient.context)
  gcpClient.crmSvc    = SetCloudResourceManagerSvc(gcpClient.crmClient)
  gcpClient.Projects  = make(map[string]GcpProject)

  // Cycle through GCP resources and generate clients for each one
  // TODO Include cases for other GCP resources
  for _, resource := range gcpResources {
    switch {
    case resource == "gke":
      gcpClient.contClient  = NewContainerClient(gcpClient.context)
      gcpClient.contSvc     = SetContainerSvc(gcpClient.contClient)
      gcpClient.GkeClusters = make(map[string]GkeCluster)
    }
  }
  return &gcpClient, nil
}

// Generate a new read only cloud resource manager http client
func NewCloudResourceManagerClientRO(ctx context.Context) *http.Client {
  crmClient, err := google.DefaultClient(ctx, cloudresourcemanager.CloudPlatformReadOnlyScope)
  h.Check(err)
  return crmClient
}

// Generate a new read only gke http client
func NewContainerClient(ctx context.Context) *http.Client {
  contClient, err := google.DefaultClient(ctx, container.CloudPlatformScope)
  h.Check(err)
  return contClient
}

func SetCloudResourceManagerSvc(crmClient *http.Client) *cloudresourcemanager.Service {
  crmSvc, err := cloudresourcemanager.New(crmClient)
  h.Check(err)
  return crmSvc
}

func SetContainerSvc(contClient *http.Client) *container.Service {
  contSvc, err := container.New(contClient)
  h.Check(err)
  return contSvc
}

// Method that obtains all the gcp projects in an org including the following
// attributes for each project: projectName, projectId, projectNumber, projectLabels
func (g *GcpClient) GetProjects() {
  filter := "lifecycleState:ACTIVE"
  request := g.crmSvc.Projects.List().Filter(filter)
  err := request.Pages(g.context, func(page *cloudresourcemanager.ListProjectsResponse) error {
    for _, project := range page.Projects {
      g.Projects[project.ProjectId] = GcpProject {
        Name:   project.Name,
        Id:     project.ProjectId,
        Number: project.ProjectNumber,
        Labels: project.Labels,
        Parent: project.Parent,
      }
    }
    return nil
  })
  h.Check(err)
}

func (g *GcpClient) GetProject(projectID string) {
  filter := fmt.Sprintf("id:%s", projectID)
  request := g.crmSvc.Projects.List().Filter(filter)
  err := request.Pages(g.context, func(page *cloudresourcemanager.ListProjectsResponse) error {
    for _, project := range page.Projects {
      g.Projects[project.ProjectId] = GcpProject {
        Name:   project.Name,
        Id:     project.ProjectId,
        Number: project.ProjectNumber,
        Labels: project.Labels,
        Parent: project.Parent,
      }
    }
    return nil
  })
  h.Check(err)
}

// Method that obtains all the gke clusters within a specified project with the
// following attributes for each cluster: clusterName, clusterZone, clusterIP of master,
// clusterLabels
func (g *GcpClient) GetGkeClusters(project string) {
  parent := fmt.Sprintf("projects/%s/locations/-", project)
  request := g.contSvc.Projects.Locations.Clusters.List(parent)
  clusters, err := request.Do()
  h.Check(err)
  for _, cluster := range clusters.Clusters {
    g.GkeClusters[cluster.Name] = GkeCluster {
      Name:   cluster.Name,
      Zone:   cluster.Zone,
      IpAddr: cluster.Endpoint,
      Labels: cluster.ResourceLabels,
    }
  }
}
