package kubeconfig

import (
  "errors"
  "k8s.io/client-go/tools/clientcmd"

  clientcmdapi "k8s.io/client-go/tools/clientcmd/api"
)

func NewKubectlConfig(kubeConfigPath string) (*clientcmdapi.Config, error) {
  if kubeConfigPath == "" {
    return nil, errors.New("kubeconfing path is empty")
  }
  kubeCtlConfig := clientcmd.LoadFromFile(kubeConfigPath)
  return kubeCtlConfig, nil
}
