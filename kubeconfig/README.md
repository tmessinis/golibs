# About
Library that allows you to import and programmatically access a kubenetes configuration file and get context information for a multi kubernetes setup.

## Example usage
Print out your current kubernetes context:
```
package main

import (
  "os"
  "fmt"
  "path/filepath"
  kubectx "bitbucket.org/tmessinis/golibs/kubeconfig"
  h "bitbucket.org/tmessinis/golibs/gohelpers"
)

func main() {
  kubeconfig := filepath.Join(
       os.Getenv("HOME"), ".kube", "config",
  )
  kc, err := kubectx.NewKubectlConfig(kubeconfig)
  h.Check(err)
  fmt.Println(kc.CurrentContext)
}
```
