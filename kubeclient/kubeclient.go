package kubeclient

import (
  "os"
  "errors"
  "path/filepath"
  "k8s.io/client-go/kubernetes"
  "k8s.io/client-go/tools/clientcmd"

  kubectx "bitbucket.org/tmessinis/golibs/kubeconfig"
  metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
  restclient "k8s.io/client-go/rest"
  corev1 "k8s.io/client-go/kubernetes/typed/core/v1"
  h "bitbucket.org/tmessinis/golibs/gohelpers"
  _ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
)

type KubeClient struct {
  kubeConfigPath string
  kubeConfig     *restclient.Config
  clientSet      *kubernetes.Clientset
  coreV1Api      corev1.CoreV1Interface
  CurrentContext string
  KubeClusters   map[string]*KubeCluster
}

type KubeCluster struct {
  Name        string
  Url         string
  Pods        []string
  Services    []string
  Deployments []string
}

func removeKubeConfig(path string) {
  err := os.Remove(path)
  h.Check(err)
}

func NewKubeClient(kubeConf string) (*KubeClient, error) {
  var kubeConfigPath string

  if kubeConf == "" {
    return nil, errors.New(
      "Specify whether to use a the default kubeconfig or create a temp one",
    )
  }

  switch {
  case kubeConf == "temp": // Create temporary kubecontext file
    kc, err := os.Create("/tmp/kubeconf_tmp")
    h.Check(err)
    kc.Close()

    // Set $KUBECONTEXT environment variable
    os.Setenv("KUBECONFIG", "/tmp/kubeconf_tmp")
    kubeConfigPath = "/tmp/kubeconf_tmp"
  case kubeConf == "default": // Use the default kubecontext file
    kubeConfigPath = filepath.Join(
         os.Getenv("HOME"), ".kube", "config",
    )
  }

  kubeClient, err := New(kubeConfigPath)
  h.Check(err)
  return kubeClient, nil
}

func New(kubeConfigPath string) (*KubeClient, error) {
  if kubeConfigPath == "" {
    return nil, errors.New("kubeconfig path is empty")
  }
  kubeClient := KubeClient{kubeConfigPath: kubeConfigPath}
  kubeClient.kubeConfig   = SetupKubeConfig(kubeClient.kubeConfigPath)
  kubeClient.clientSet    = SetupKubeClientSet(kubeClient.kubeConfig)
  kubeClient.coreV1Api    = SetupCoreV1Api(kubeClient.clientSet)
  kubeClient.KubeClusters = make(map[string]*KubeCluster)

  return &kubeClient, nil
}

func SetupKubeConfig(kubeConfigPath string) *restclient.Config {
  kubeConfig, err := clientcmd.BuildConfigFromFlags("", kubeConfigPath)
  h.Check(err)
  return kubeConfig
}

func SetupKubeClientSet(kubeConfig *restclient.Config) *kubernetes.Clientset {
  clientSet, err := kubernetes.NewForConfig(kubeConfig)
  h.Check(err)
  return clientSet
}

func SetupCoreV1Api(clientSet *kubernetes.Clientset) corev1.CoreV1Interface {
  coreV1Api := clientSet.CoreV1()
  return coreV1Api
}

func (k *KubeClient) GetCurrentContext() {
  var kubeMasterUrl string

  kubeContext, err := kubectx.NewKubectlConfig(k.kubeConfigPath)
  h.Check(err)

  k.CurrentContext = kubeContext.CurrentContext

  for _, cluster := range kubeContext.Clusters {
    if cluster.Name == k.CurrentContext {
      kubeMasterUrl = cluster.Cluster.Server
    }
  }
  k.KubeClusters[k.CurrentContext] = &KubeCluster {
    Name: k.CurrentContext,
    Url:  kubeMasterUrl,
  }
}

func (k *KubeClient) GetServices(namespace string) {
  var ns string

  if namespace == "" {
    ns = "default"
  } else {
    ns = namespace
  }
  services, err := k.coreV1Api.Services(ns).List(metav1.ListOptions{})
  h.Check(err)

  if len(services.Items) > 0 {
    for _, svc := range services.Items {
      k.KubeClusters[k.CurrentContext].Services = append(
        k.KubeClusters[k.CurrentContext].Services,
        svc.Name,
      )
    }
  }
}
