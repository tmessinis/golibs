# About
This library allows you to communicate with a kubernetes cluster with respect to the current selected context. If the string "temp" is passed to the `NewKubeClient` function a temporary kubernetes configuration file will be created at `/tmp/kubeconf_tmp` and the `KUBECONFIG` environment variable will be set to that path.

### TO DO
Include function to change context.

## Example usage
```
package main

import (
  "fmt"
  "bitbucket.org/tmessinis/golibs/kubeclient"
  h "bitbucket.org/tmessinis/golibs/gohelpers"
)

func main() {
  kubeClient, err := kubeclient.NewKubeClient("default")
  h.Check(err)
  kubeClient.GetCurrentContext()
  kubeClient.GetServices("")
  cc := kubeClient.CurrentContext
  fmt.Println("Current cluster:")
  fmt.Println(cc)
  fmt.Println("")
  fmt.Println("Cluster services:")
  fmt.Println(kubeClient.KubeClusters[cc].Services)
}
```
