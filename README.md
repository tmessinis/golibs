# About
Repository that keeps a collection of go libraries that can help with the authenticating and communicating via REST to GCP and Kubernetes clusters. Within each library there's a readme with some basic example usage.

## gcpclient
This library will create a GcpClient object that using the user's default GCP credentials. With it you can obtain information regarding an org's projects and other GCP resources.

## kubeconfig
Library that takes the path of a kubernetes configuration file and generates an object that reflects the fields and values found in that configuration. You can then easily access each field with dot notation.

## kubeclient
Library that allows programmatic access via REST api to a kubernetes cluster. As of 04/03/2019 the versions of the `k8s.io/api`, `k8s.io/apimachinery` and `k8s.io/client-go` libraries will have to set to the following in `go.mod`:
```
require (
  k8s.io/api kubernetes-1.13.4
  k8s.io/apimachinery kubernetes-1.13.4
  k8s.io/client-go v10.0.0
)
```
