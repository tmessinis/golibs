package gohelpers

import (
  "os"
  "io"
  "fmt"
  "log"
  "bytes"
  "os/exec"
  "text/template"
)

func init() {
  // Change the device for logging to stdout
  log.SetOutput(os.Stdout)
}

// Check if it's an error and log the output to stdout
func Check(err error) {
  if err != nil {
    log.Println(err)
  }
}

// StringsInSlice checks if a slice exits within a slice of strings
func StringInSlice(aString string, aSlice []string) bool {
  for _, stringElem := range aSlice {
    if aString == stringElem {
      return true
    }
  }
  return false
}

// ExecCmd will shell out a command and its arguments and return the stdout and
// stderr
func ExecCmd(aCmd string, arg ...string) (bytes.Buffer, bytes.Buffer) {
  var stdout, stderr bytes.Buffer
  cmd := exec.Command(aCmd, arg...)
  cmd.Stdout = &stdout
  cmd.Stderr = &stderr
  err := cmd.Run()
  Check(err)

  return stdout, stderr
}

// SwitchGkeCtx will use gcloud to get the gke credentials for a cluster and
// change the k8s context
func SwitchGkeCtx(project string, cluster string, region string) (bytes.Buffer, bytes.Buffer) {
  stdout, stderr := ExecCmd(
    "gcloud",
    "container",
    "clusters",
    "get-credentials",
    cluster,
    fmt.Sprintf("--project=%s", project),
    fmt.Sprintf("--region=%s", region),
  )
  return stdout, stderr
}

// Copy copies a file to a destination. The function checks to see if the file
// makes sure that it's a regular file
func Copy(src, dst string) (int64, error) {
  sourceFileStat, err := os.Stat(src)
  Check(err)

  if !sourceFileStat.Mode().IsRegular() {
    return 0, fmt.Errorf("%s is not a regular file", src)
  }

  source, err := os.Open(src)
  Check(err)
  defer source.Close()

  destination, err := os.Create(dst)
  Check(err)
  defer destination.Close()
  nBytes, err := io.Copy(destination, source)
  return nBytes, err
}

// StringFormatSliceExpander allows for a string that includes the string
// formatting operators, and the values that are passed to it belong to a slice
// of strings that was dynamically generated and the values are not known in
// advance
func StringFormatSliceExpander(values []string, strToFmt string) string {
  params := make([]interface{}, 0, len(values))
  for _, val := range values {
    params = append(params, val)
  }

  return fmt.Sprintf(strToFmt, params...)
}

// GenTemplate will populate a text template with the data from a struct. You can
// include several templates which might depend on one another
func GenTemplate(tmplDefinition, tmplFilePathOut string, data interface{}, tmplPaths ...string) error {
	var err error
	var tmplVarsOut *template.Template
	var tmplVarsFile *os.File

	tmplVarsOut, err = template.ParseFiles(tmplPaths...)
	Check(err)
	tmplVarsFile, err = os.OpenFile(tmplFilePathOut, os.O_RDWR|os.O_CREATE, 0755)
	Check(err)
	defer tmplVarsFile.Close()

	return tmplVarsOut.ExecuteTemplate(tmplVarsFile, tmplDefinition, data)
}

// FileExists checks to see if a file exists in that path and returns false if it
// does not and true if it does
func FileExists(filePath string) bool {
  if _, err := os.Stat(filePath); os.IsNotExist(err) {
		return false
	}

  return true
}
